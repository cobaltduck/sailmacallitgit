/*
 * QUnit test file for Sailboat Whatchamcallit Flowchart
 *
 * Created by Wade J. Love
 *
 * Sailboat Whachamacallit Flowchart, its source code, and all related images
 * are released under the Creative Commons Attribution-NonCommercial-ShareAlike 
 * 4.0 International License. (http://creativecommons.org/licenses/by-nc-sa/4.0/)
 *
 * This means you are free to share and adapt this work as long as you give appropriate credit,
 * do not use any material for commercial use, and distribute your contributions under the same
 * license.  See the links for details.
 *
 * These unit tests should not be exported to server!  However, due to the need to read JSON files,
 * they must be run from a localhost server, and not from file://
 *
 */

// NOTE: owing to the checkImages() function, an image's src can get replaced with default.svg,
// so all tests that check image src attribute are skipped.
		

// document.ready
// no test needed (nor possible)

// pageSetUp(key)
// Tests that events are added to needed DOM elements
QUnit.test("pageSetUp adds events to several elements", function( assert ) {
	pageSetUp('test');
	var windowEvents = $._data($(window)[0]).events;
	
	assert.ok(windowEvents.hasOwnProperty('orientationchange'), "Window has orientation change event");
	
});

// getKey
// TODO: Need to find a way to mock window.location


// refreshViaKey(key)
// Attempting to test would cause url to change, so skipped

// loadConfig(key)
// Tests that a JSON file can be read, parsed, and translated into correct elements
QUnit.test("loadConfig adds elements to the DOM from a JSON file", function( assert ) {
	loadConfig('test');
	
	var done = assert.async();
	setTimeout(function() {
		assert.equal($('#explainer').html(), 'TEST EXPLAINER', 'html is set for explainer element');
		assert.equal($('#question').html(), 'Test question?', 'html is set for question element');
		done();
	}, 250);
	// remainder tested under other functions
});

// tests that a JSON failure results in failNicely
QUnit.test("loadConfig fails nicely when JSON errors", function( assert ) {
	loadConfig('fakeKey');
	
	var done = assert.async();
	setTimeout(function() {
		assert.equal($('#explainer').html(), 'We\'re sorry, but there seems to be an error in our site right now.', 'html is set for explainer element');
		assert.equal($('#question').html(), 'Please try again, or come back later.  If the problem persists, please contact us and let us know.', 'html is set for question element');
		done();
	}, 250);
	// remainder tested under other functions
});

// buildBreadcrumbs(crumbyArray)
QUnit.test("buildBreadCrumbs add elements to the DOM from JSON", function( assert ) {
	var testBcData = [
	    { "key":"test", "text":"Test BC" },
	    { "key":"sample", "text":"Sample BC"}
	];
	
	buildBreadcrumbs(testBcData);
	
	var done = assert.async();
	setTimeout(function() {
		// check that there are two span, two img
		var childSpanCount = $('#breadcrumbWrapper').children('span').length;
		var childImgCount = $('#breadcrumbWrapper').children('img').length;
		
		assert.equal(childSpanCount, 2, 'There are two <span> elements in the breadcrumbs');
		assert.equal(childImgCount, 2, 'There are two <img> elements in the breadcrumbs');
		
		// check that each span has the correct class and content
		assert.ok($('#breadcrumbWrapper').children('span').eq(0).hasClass('breadcrumb'), 'First <span> has breadcrumb class');
		assert.ok($('#breadcrumbWrapper').children('span').eq(1).hasClass('breadcrumb'), 'Second <span> has breadcrumb class');
		assert.equal($('#breadcrumbWrapper').children('span').eq(0).html(), 'Test BC', 'First <span> has correct html content');
		assert.equal($('#breadcrumbWrapper').children('span').eq(1).html(), 'Sample BC', 'Second <span> has correct html content');
		
		// check that each img has the correct class and content
		assert.ok($('#breadcrumbWrapper').children('img').eq(0).hasClass('aftercrumb'), 'First <img> has aftercrumb class');
		assert.ok($('#breadcrumbWrapper').children('img').eq(1).hasClass('aftercrumb'), 'Second <img> has aftercrumb class');
		//assert.equal($('#breadcrumbWrapper').children('img').eq(0).attr('src'), 'svg/chevron.svg', 'First <span> has correct html content');
		//assert.equal($('#breadcrumbWrapper').children('img').eq(1).attr('src'), 'svg/chevron.svg', 'Second <span> has correct html content');
		
		// for events, we are only testing the first in the array
		var bcEvents = $._data($('.breadcrumb')[0]).events;
		assert.ok(bcEvents.hasOwnProperty('click'), "breadcrumb has click event");
		assert.ok(bcEvents.hasOwnProperty('keyup'), "breadcrumb has keyup event");
		
		done();
	}, 250);
});

QUnit.test("buildBreadCrumbs fails nicely when array is null", function( assert ) {
	var testBcData = null;
	
	buildBreadcrumbs(testBcData);
	
	var done = assert.async();
	setTimeout(function() {
		assert.equal($('#bcError').html(), 'Unable to load breadcrumbs', 'Adds an error message in place of breadcrumbs');
		done();
	}, 250);
});

// setupBreadcrumbs()
QUnit.test("setUpBreadcrumbs creates a proper height", function( assert ) {
	var testBcData = [
	    { "key":"test", "text":"Test BC" },
	    { "key":"sample", "text":"Sample BC"}
	];
	
	// Going through builder to get to setup, so that is sets values we need, creates elements
	buildBreadcrumbs(testBcData);
	
	assert.equal($('.aftercrumb').height(), 16, 'Height is the sum of font and padding');
});

// buildSteps(stepArray, key)
QUnit.test("buildSteps adds element to DOM from JSON", function ( assert ) {
	var testStepData = [
		{ "key":"next1", "text":"Test ONE" },
		{ "key":"next2", "text":"Test TWO" }
	];
	
	buildSteps(testStepData, 'fakeKey');
	
	var done = assert.async();
	setTimeout(function() {
		// check that there are two figures, each with a img and caption child, all with correct class
		var childFigureCount = $('#stepWrapper').children('figure').length;
		assert.equal(childFigureCount, 2, 'There are two <figure> elements in the steps');
		
		$('#stepWrapper').children('figure').each(function(){
			assert.ok($(this).hasClass('stepFigure'), 'Each <figure> has the correct class')
			
			assert.equal($(this).children('img').length, 1, 'Each figure has an <img> child');
			assert.equal($(this).children('figcaption').length, 1, 'Each figure has an <figcaption> child');
			
			assert.ok($(this).children('img').eq(0).hasClass('stepImage'), 'Each <img> has the correct class');
			assert.ok($(this).children('figcaption').eq(0).hasClass('stepCaption'), 'Each <img> has the correct class');
		});
		
		assert.equal($('#stepWrapper').children('figure').eq(0).children('figcaption').eq(0).html(), 'Test ONE', 'First figure has correct caption text');
		assert.equal($('#stepWrapper').children('figure').eq(1).children('figcaption').eq(0).html(), 'Test TWO', 'Second figure has correct caption text');
		//assert.equal($('#stepWrapper').children('figure').eq(0).children('img').eq(0).attr('src'), 'svg/next1.svg', 'First figure has correct img src');
		//assert.equal($('#stepWrapper').children('figure').eq(1).children('img').eq(0).attr('src'), 'svg/next2.svg', 'Second figure has correct img src');
		
		done();
		
	}, 250);
});

QUnit.test("buildSteps creates proper elements at last step in a flow", function( assert ) {
	var testStepData = null;
	
	buildSteps(testStepData, 'fakeKey');
	
	var done = assert.async();
	setTimeout(function() {
		// this time, there should be no figure, just an <img> and a <p>.
		var childFigureCount = $('#stepWrapper').children('figure').length;
		var childImgCount = $('#stepWrapper').children('img').length;
		var childParaCount = $('#stepWrapper').children('p').length;
		assert.equal(childFigureCount, 0, 'There are no <figure> elements in the steps');
		assert.equal(childImgCount, 1, 'There is one <img> element in the steps');
		assert.equal(childParaCount, 1, 'There is one <p> element in the steps');
		
		assert.ok($('#stepWrapper').children('img').eq(0).hasClass('endFlowImage'), 'The image has the correct class');
		//assert.equal($('#stepWrapper').children('img').eq(0).attr('src'), 'svg/fakeKey.svg', 'The image has the correct source');
		done();
		
	}, 250);
});

// checkImages()
// Tests skipped - this is a failure function, and the "test" folder has no images available anyway

// failNicely()
QUnit.test("failNicely sets certain elements to friendly errors messages", function( assert ) {
	failNicely();
	
	var done = assert.async();
	setTimeout(function() {
		assert.equal($('#explainer').html(), 'We\'re sorry, but there seems to be an error in our site right now.', 'html is set for explainer element');
		assert.equal($('#question').html(), 'Please try again, or come back later.  If the problem persists, please contact us and let us know.', 'html is set for question element');
		assert.equal($('#bcError').html(), 'Error', 'html is set for bcError element');
		assert.equal($('#stepWrapper').html(), '', 'html is set for stepWrapper element');
		done();
	}, 250);
});

// setUpFocusStyles()
QUnit.test("setUpFocusStyles adds event listeners", function( assert ) {
	setUpFocusStyles();
	
	var bcEvents = $._data($('.breadcrumb')[0]).events;
	var stepEvents = $._data($('.stepFigure')[0]).events;
	
	assert.ok(bcEvents.hasOwnProperty('focus'), "Breadcrumb has focus event");
	assert.ok(bcEvents.hasOwnProperty('blur'), "Breadcrumb has blur event");
	assert.ok(stepEvents.hasOwnProperty('focus'), "step has focus event");
	assert.ok(stepEvents.hasOwnProperty('blur'), "step has blur event");
});

/* is not working - programmatic focus here does not add the class for some reason
QUnit.test("setUpFocusStyles create CSS changes", function( assert ) {
	setUpFocusStyles();
	
	assert.notOk($('.breadcrumb#0').hasClass('isFocus'), 'isFocus class not present initially on breadcrumb');
	$('.breadcrumb#0').focus(); // simulate item coming in to focus
	assert.ok($('.breadcrumb#0').hasClass('isFocus'), 'focus event adds isFocus class to breadcrumb');
	$('.breadcrumb#0').blur(); // simulate item coming in to focus
	assert.notOk($('.breadcrumb#0').hasClass('isFocus'), 'blur event removes isFocus class from breadcrumb');
	
	// TODO: focus on stepFigure adds the class to the children, not the main element
	
}); */