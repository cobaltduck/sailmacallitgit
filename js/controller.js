/* 
 * JavaScript controller for Sailboat Whachamacallit Flowchart
 *
 * Created by Wade J. Love
 *
 * Sailboat Whachamacallit Flowchart, its source code, and all related images
 * are released under the Creative Commons Attribution-NonCommercial-ShareAlike 
 * 4.0 International License. (http://creativecommons.org/licenses/by-nc-sa/4.0/)
 *
 * This means you are free to share and adapt this work as long as you give appropriate credit,
 * do not use any material for commercial use, and distribute your contributions under the same
 * license.  See the links for details.</p>
 *
 */

$(document).ready(function() {
	var key = getKey();
	if (!key) {
		key = 'home';
	}
	
	loadConfig(key);
	pageSetUp(key);
		
});

/**
 * Basic clean-up and prep tasks to perform on load
 */
function pageSetUp(key) {
	// allows changing the CSS classes on device rotate
	$(window).on( "orientationchange", function(event) {
		$('body').hide();
		setTimeout(function() {
			$('body').show(0);
		}, 100);
		setupBreadcrumbs();
	});
	
	setTimeout(function() {
		setUpFocusStyles();
		$('.breadcrumb#' + key).focus();
	}, 250);

}

/**
 * Retrieves the "key" query string from the URL
 * @returns the value of the key, or false if no key is present
 */
function getKey() {
	var paramStr = window.location.search.substring(1);  // i.e. everything after the ?
	var paramList = paramStr.split('&');
	for (var i = 0; i < paramList.length; i++) {
        var param = paramList[i].split('=');
        if (param[0] == 'key')        {
            return param[1];
        }
    }
	return false;
}

/**
 * Sets the query string to the "key" and refreshes the page
 */
function refreshViaKey(key) {
	var url = window.location.pathname + "?key="+key;
	window.location.href = url;
}

/**
 * This is the meat and potatoes of this app.
 * This loads the content for the current step in the flow
 * from an appropriate json config file, then builds
 * the page accordingly
 */
function loadConfig(key) {
	// console.log('Loading config for key: ' + key);  // Only uncomment for local testing
	
	var jsonUrl = './components/' + key + '.json';
	$.getJSON(jsonUrl, function(data) {
		if (data) {
			var isFinal = !data.steps;
			$('#explainer').html(data.explainer);
			$('#question').html(data.question);
			buildBreadcrumbs(data.breadcrumbs, isFinal);
			buildSteps(data.steps, key);
		}
		else {
			console.log("JSON config read, but no data");
			failNicely();
		}
		
	})
	.fail(function(jqxhr, textStatus, error) {
		console.log("JSON config could not be read with messages: " + textStatus + " and " + error);
		failNicely();
	});
}

/**
 * Builds the HTML for the breadcrumb section
 */
function buildBreadcrumbs(crumbyArray, isFinal) {
	if (crumbyArray) {
		// clear existing default
		$('#breadcrumbWrapper').html('');
		for (var i=0; i<crumbyArray.length; i++) {
			var crumbyItem = crumbyArray[i];
			// build one span per item
			var crumbyElement =	$('<span/>', {
				class: 'breadcrumb',
				id: crumbyItem.key,
				text: crumbyItem.text,
				tabindex: 0
			});
			$('#breadcrumbWrapper').append(crumbyElement);
			
			// and add the chevron ( or pipe if the final step in a flow )
			var crumbySeparator = (isFinal && i == crumbyArray.length-1) ?
					'pipe' : 'chevron'
			var crumbyChevron = $('<img/>', {
				class: 'aftercrumb',
				'aria-hidden': 'true',
				alt: '',
				height: '100%',
				src: 'images/' + crumbySeparator + '.svg'
			});
			$('#breadcrumbWrapper').append(crumbyChevron);
		}
		
		// set breadcrumbs to be links to given key
		$('.breadcrumb').click(function(event) {
			refreshViaKey(event.target.id);
		});
		$('.breadcrumb').keyup(function(event) {
			if(event.which === 13) {
				refreshViaKey(event.target.id);
			}
		});
		
	}
	else {
		$('#bcError').html('Unable to load breadcrumbs');
	}
	
	setupBreadcrumbs();
}

/**
 * After building breadcrumbs or loading from default,
 * call this function to adjust styling
 */
function setupBreadcrumbs() {
	// set height of breadcrumb chevron separators
	var bcHeight = parseInt($('.breadcrumb').css('font-size'));
	var bcPad = parseInt($('.breadcrumb').css('padding'));
	var finalHeight = bcHeight + bcPad;
	$('.aftercrumb').height(finalHeight);
}

/**
 * Build the HTML for the steps sections
 * @param stepArray
 */
function buildSteps(stepArray, key) {
	// clear existing default
	$('#stepWrapper').html('');
	if (stepArray) {
		for (var i=0; i<stepArray.length; i++) {
			var stepItem = stepArray[i];
			
			var figClass = 'stepFigure';
			var stepText = stepItem.text
			// This should be temp. while things are under construction
			if (stepItem.disabled) {
				figClass += ' disabled';
				stepText += ' (Under construction, not yet available.)';
			}
			// under temp. construction code
			
			// HTML5 - We're building a figure, with an image and a figcaption
			// figure
			var stepFigure = $('<figure/>', {
				class: figClass,
				id: stepItem.key,
				tabindex: 0
			});
			
			// image
			var stepImage = $('<img/>', {
				class: 'stepImage',
				id: stepItem.key,
				'aria-hidden': 'true',
				alt: '',
				src: 'svg/'+stepItem.key+'.svg',
			}); 
			stepFigure.append(stepImage);
			
			// caption below image
			var stepCaption = $('<figcaption/>', {
				class: 'stepCaption',
				id: stepItem.key,
				text: stepText
			});
			stepFigure.append(stepCaption);
			
			// pull it together
			$('#stepWrapper').append(stepFigure);
		}
		
		// set steps to be links to given key
		$('.stepFigure:not(.disabled)').click(function(event) {
			refreshViaKey(event.target.id);
		});
		$('.stepFigure:not(.disabled)').keyup(function(event) {
			if(event.which === 13) {
				refreshViaKey(event.target.id);
			}
		});
	}
	else { // assume end of flow, display image for this step and end-flow text 
		// image
		var stepImage = $('<img/>', {
			class: 'endFlowImage',
			'aria-hidden': 'true',
			alt: '',
			src: 'svg/'+key+'.svg',
		});
		
		var homeLink = $('<a/>', {
			href: 'index.html?key=home',
			html: 'start over'
		});
		
		var endFlowPara = $('<p/>', {
			html: 'You have reached the end of this flow.  Use the bread-crumb links above to back-track, or '
		});
		
		endFlowPara.append(homeLink).append('.');
		$('#stepWrapper').append(stepImage);
		$('#stepWrapper').append(endFlowPara);
	}
	
	// wait a moment for the above images to render, then replace broken ones
	setTimeout(function() {
		checkImages();
	}, 1000);
}

/**
 * Replace any broken images with the default "coming soon" image
 */
function checkImages() {
	// Thank you Devon on Stack Overflow! http://stackoverflow.com/a/93017/241291
	$('img').each(function() {
		if (!this.complete || typeof this.naturalWidth == "undefined" || this.naturalWidth == 0) {
			$(this).attr('src', 'svg/default.svg');
	    }
	});
}

/**
 * Should anything go wrong in the loading of a JSON config,
 * call this function to provide a friendly warning to the user.
 */
function failNicely() {
	$('#explainer').html('We&apos;re sorry, but there seems to be an error in our site right now.');
	$('#question').html('Please try again, or come back later.  If the problem persists, please contact us and let us know.');
	$('#bcError').html('Error');
	$('#stepWrapper').html('');
}

/**
 * Add and remove style classes as breadcrumbs and steps gain and loose focus 
 */
function setUpFocusStyles() {
	$('.breadcrumb').focus(function() {
		$(this).addClass('isfocus');
	});
	$('.breadcrumb').blur(function() {
		$(this).removeClass('isfocus');
	});
	
	$('.stepFigure').focus(function() {
		$(this).children('.stepImage').addClass('isfocus');
		$(this).children('.stepCaption').addClass('isfocus');
	});
	$('.stepFigure').blur(function() {
		$(this).children('.stepImage').removeClass('isfocus');
		$(this).children('.stepCaption').removeClass('isfocus');
	});
}