All SVG images in this directory were
Created by Wade J. Love

Sailboat Whachamacallit Flowchart, its source code, and all related images
are released under the Creative Commons Attribution-NonCommercial-ShareAlike 
4.0 International License. (http://creativecommons.org/licenses/by-nc-sa/4.0/)

This means you are free to share and adapt this work as long as you give appropriate credit,
do not use any material for commercial use, and distribute your contributions under the same
license.  See the links for details.